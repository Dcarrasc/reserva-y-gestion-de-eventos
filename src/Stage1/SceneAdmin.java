package Stage1;

import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class SceneAdmin {
    public static String nombre_evento;
    public LocalDate fecha;
    public String fecha_string;
    public double precio;
    public ArrayList<Evento> eventos;
    public Evento evento;
    public Horario horario;

    @FXML
    private TextField add_event;

    @FXML
    private TextField add_fecha;

    @FXML
    private TextField add_precio;

    @FXML
    private MenuItem menu_back;

    @FXML
    private MenuItem menu_change;

    @FXML
    private Button btn_add;

    @FXML
    private Button mostrar_evento;


    @FXML
    void press_ModificarEvento(ActionEvent event) {

    }

    @FXML
    void press_VolverAtras(ActionEvent event) {

    }

    @FXML
    void press_btn_addEvento(ActionEvent event) {
        nombre_evento=add_event.getText();
        fecha_string=add_fecha.getText();
        precio=Double.parseDouble(add_precio.getText());
        LocalDate localDate1 = LocalDate.parse(fecha_string, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        horario=new Horario(localDate1);
        evento=new Evento(horario);
        eventos.add(evento);


    }

    @FXML
    void press_MostrarEvento(MouseEvent event) {
        for(int i=0 ; i < eventos.size() ;i++){
            System.out.println("Evento numero"+i +" "+ eventos.get(i).getPrecio());
            System.out.println("Evento numero"+i +" "+ eventos.get(i).getHorario());
            System.out.println("Evento numero"+i +" "+ eventos.get(i).getNombre_evento());

        }
    }

    public ArrayList<Evento> getEventos(){return eventos;}

    @FXML
    void add_fechaEvento(ActionEvent event) {

    }

    @FXML
    void add_nombreEvento(ActionEvent event) {

    }

    @FXML
    void add_precioBoleto(ActionEvent event) {

    }


}

