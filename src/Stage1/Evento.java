package Stage1;

import java.util.ArrayList;

public class Evento {

    public Horario horario;
    public String nombre_evento;
    public double precio;


    public Evento(Horario horario){
        //Inicializar atirbutos de la clase Evento
        this.horario=horario;
    }

    public void setPrecio(double precio){this.precio=precio;}
    public double getPrecio(){return precio;}

    public void setHorario(Horario horario){this.horario=horario;}
    public Horario getHorario(){return horario;}

    public void setNombre(String nombre){nombre_evento=nombre;}
    public String getNombre_evento(){return nombre_evento;}


}
