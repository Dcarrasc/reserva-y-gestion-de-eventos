# Proyecto Reserva y gestión de Eventos

## Tabla de información
***
1. [Información General](#Información-general)
2. [Tecnología](#tecnologías)
3. [Ejecución del programa](#Ejecución-del-programa)
4. [Colaboraciones](#Colaboraciones)
***

### Información General

Saludos a la persona que esté leyendo este documento, este te ayudará a comprender
de mejor manera nuestro programa, te guiaremos en cada paso.

Este programa se compone de los siguientes archivos:
+ La carpeta "**Src**" sinónimo de Source
+ La carpeta "**Stage1**" Correspondiente a la etapa 1


### Tecnologías
***
A list of technologies used within the project:
* [Intellij IDEA](https://www.jetbrains.com/es-es/idea/): Version 2020.1
* [GitLab](https://gitlab.com/Dcarrasc/tareapoo.git): Version 13.12.0
* [MobaExterm](https://mobaxterm.mobatek.net): Version 21.1
* [JavaFx](https://openjfx.io): Version 14.


## Ejecución del Programa
***
Para gestionar nuestro programa utilizamos un recurso llamado "GitLab" que con
algunos conocimientos de "GIT" puedes acceder fácilmente.
(De todos modos explicaremos paso a paso)

Para compilar nuestro programa primero tendrás que estar dentro de una carpeta
local y hacer un **Clone** desde nuestro repositorio GIT con el siguiente:

```
$ git clone https://gitlab.com/Dcarrasc/reserva-y-gestion-de-eventos.git
```

Luego desde tu terminal acceder a la carpeta que acabas de **"clonar"** en este caso
se creará una carpeta con el nombre "tareapoo"

##Colaboraciones
***
Integrantes del equipo:
+ David Carrasco
+ Javier Delgado
+ Yohans jara  
+ Francisco lopez
