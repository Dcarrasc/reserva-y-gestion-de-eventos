public class Usuario {                                          // Clase para crear los usuarios del programa
    private String nombreUsuario;                               // nombre del usuario
    private String claveUsuario;                                // clave del usuario 
    private int tipoUsuario;                                    // tipo de usuario: 1=cliente   2=administrador

    public Usuario(String nombre, String clave, int tipo){      // Constructor de la clase
        this.nombreUsuario = nombre;
        this.claveUsuario = clave;                              // guarda la clave encriptada del usuario
        this.tipoUsuario = tipo;
    }

    public String getNombreUsuario(){                           // retorna el nombre del usuario
        return this.nombreUsuario;
    }

    public String getClaveUsuario(){                            // retorna la clave encriptada del usuario
        return this.claveUsuario;
    }

    public int getTipoUsuario(){                                // retorna el tipo de usuario
        return this.tipoUsuario;
    }
}
