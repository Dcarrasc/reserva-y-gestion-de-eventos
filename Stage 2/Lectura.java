import java.io.File;                        // Librerias usadas
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Scanner;

public class Lectura {                  // Clase para leer los archivos
    public Lectura() {}                 // Constructor vacio, ya que es una clase hecha solamente por metodos estaticos

    // Metodo para leer el archivo de texto con los usuarios registrados
    public static void leerArchivoUsuarios(String nombreArchivo, ArrayList <Usuario> usu){
        File archivoTexto = new File(nombreArchivo);
        Scanner s;
        try {
            s = new Scanner(archivoTexto);
            while(s.hasNextLine()){     // Para revisar si hay una siguiente linea
                String linea = s.nextLine();
                Scanner sl = new Scanner(linea);
                sl.useDelimiter("\\s*,\\s*");   // Usa la coma de separador
                String nombreUsuario = sl.next();   // Guarda los datos del archivo
                String claveUsuario = sl.next();
                int tipoUsuario = Integer.parseInt(sl.next());
                usu.add(new Usuario(nombreUsuario, claveUsuario, tipoUsuario)); // Crea el usuario y lo agrega al array de usuarios
            }
            s.close();      // Cierra el archivo
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    // No funciono este metodo y preferimos omitirlo
    /*
    public static void leerArchivoReservas(String nombreArchivo, ArrayList <Reserva> res, ArrayList <Evento> even, ArrayList <Usuario> usu){
        File archivoTexto = new File(nombreArchivo);
        Scanner s;
        try {
            s = new Scanner(archivoTexto);
            while(s.hasNextLine()){
                String linea = s.nextLine();
                Scanner sl = new Scanner(linea);
                sl.useDelimiter("\\s*,\\s*");
                int idReserva = Integer.parseInt(sl.next());
                String nombreCliente = sl.next();
                String nombreEvento = sl.next();
                String fechaEvento = sl.next();
                String horaEvento = sl.next();
                int precioEvento = Integer.parseInt(sl.next());
                int numeroAsiento = Integer.parseInt(sl.next());
                int posicionEvento = 0;
                for(int i=0;i<even.size();i++){
                    if(even.get(i).getNombreEvento().equals(nombreEvento)){
                        posicionEvento = i;
                    }
                }
                LocalDate nuevaFecha = LocalDate.parse(fechaEvento);
                LocalTime nuevaHora = LocalTime.parse(horaEvento);
                int posicionUsuario = 0;
                for(int j=0; j<usu.size();j++){
                    if(usu.get(j).getNombreUsuario().equals(nombreCliente)){
                        posicionUsuario = j;
                    }
                }
                res.add(new Reserva(even.get(posicionEvento), nuevaFecha, nuevaHora, numeroAsiento, usu.get(posicionUsuario)));
                //Reserva.reservar(even, even.get(posicionEvento), nuevaFecha, nuevaHora, numeroAsiento);
            }
            s.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    */
}
