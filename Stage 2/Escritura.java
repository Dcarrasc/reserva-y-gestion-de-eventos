import java.io.*;                   // Librerias usadas
import java.util.ArrayList;

public class Escritura {            // Clase para ecritura de archivos

    public Escritura() {}           // No posee constructor, ya que esta hecha solamente con metodos estaticos

    public static void EscribirArchivoUsuarios(ArrayList <Usuario> usu){    // Metodo para escribir el archivo de usuarios
        FileWriter archivoTexto = null;
        PrintWriter pw = null;
        try {
            archivoTexto = new FileWriter("Usuarios.txt");
            pw = new PrintWriter(archivoTexto);
            for(int i=0;i<usu.size();i++){  // Se recorre el arreglo de usuarios para escribir linea por linea en el archivo
                pw.println(usu.get(i).getNombreUsuario() + ", " + usu.get(i).getClaveUsuario() + ", " + usu.get(i).getTipoUsuario());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(null != archivoTexto){
                    archivoTexto.close();       // Se cierra el archivo
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

    }
        
}
