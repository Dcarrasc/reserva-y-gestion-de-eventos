import java.time.LocalDate;                                             // Librerias que se usaron
import java.time.LocalTime;
import java.util.ArrayList;

public class Horario {                                                  // Clase para definir los horarios
    private LocalDate fecha;                                            // La fecha 
    private LocalTime hora;                                             // La hora
    private ArrayList <Asiento> asientos;                               // Arreglo con los asientos que hay en el horario

    public Horario(LocalDate fecha, LocalTime hora, int cantAsientos){  // Constructor
        this.fecha = fecha;
        this.hora = hora;
        asientos = new ArrayList <Asiento> ();
        for(int i=0; i<cantAsientos;i++){                               // Se crea el array de asientos con la cantidad de asientos indicada
            asientos.add(new Asiento(i+1, false));                      // Todos los asientos se inicializan como LIBRES
        }
    }

    public LocalDate getFecha(){                                        // Metodo para obtener la fecha
        return this.fecha;
    }

    public LocalTime getHora(){                                         // Metodo para obtener la hora
        return this.hora;
    }

    public ArrayList<Asiento> getAsientos(){                            // Metodo para obtener el array de asientos
        return this.asientos;
    }

    public Asiento getAsientoX(int x){                                  // Metodo para obtener el asiento de numero "X"
        return this.asientos.get(x-1);
    }

    public void setAsientoX(int x){                                     // Metodo para cambiar el estado del asiento de numero "X"
        this.asientos.get(x-1).setOcupado(true);
    }

    public void printHorarioFechaHora(){                                // Metodo para imprimir la fecha y la hora
        System.out.print(fecha);                                        // fecha, hora
        System.out.print(" , ");
        System.out.print(hora);
    }

    public void printHorario(){                                         // Metodo para imprimir la fecha, la hora y el array de asientos
        System.out.print(fecha);
        System.out.print(",");                                          // fecha, hora, [(1, FALSE) (2, FALSE) (3, TRUE) ...]
        System.out.print(hora);
        System.out.print(",[");
        for(int i=0; i<asientos.size(); i++){
            asientos.get(i).printAsiento();
        }
        System.out.print("]");
    }
}
