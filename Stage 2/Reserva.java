import java.time.LocalDate;                             // Librearias usadas
import java.time.LocalTime;
import java.util.ArrayList;

public class Reserva {                                  // Clase para las reservas
    
    private static int contador = 1;                    // contador de instancias de esta clase
    private int idReserva;                              // El numero de la reserva
    private Usuario cliente;                            // El cliente que hace la reserva
    private String nombreEvento;                        // El nombre del evento
    private LocalDate fechaEvento;                      // La fecha del evento
    private LocalTime horaEvento;                       // La hora del evento
    private int precioEvento;                           // El precio del evento
    private int numeroAsiento;                          // El numero de asiento en el evento

    public Reserva(Evento e, LocalDate f, LocalTime h, int n, Usuario cli){     // Constructor
        this.idReserva = contador;
        this.nombreEvento = e.getNombreEvento();
        this.fechaEvento = f;
        this.horaEvento = h;
        this.precioEvento = e.getPrecioEvento();
        this.numeroAsiento = n;
        this.cliente = cli;
        contador++;             // Se aumenta el contador, asumiendo que ya se hizo una reserva
    }

    public int getIdReserva(){              // Metodo para obtener el id de la reserva (numero de la reserva)
        return this.idReserva;
    }

    public void printReserva(){             // Metodo para imprimir los datos de la reserva
        System.out.println("Numero de Reserva: " + idReserva);
        System.out.println("La reserva esta a nombre de: " + cliente.getNombreUsuario());
        System.out.println("Nombre del Evento: " + nombreEvento);
        System.out.print("Fecha del Evento: ");
        System.out.println(fechaEvento);
        System.out.print("Hora del Evento: ");
        System.out.println(horaEvento);
        System.out.println("Precio del Evento: " + precioEvento);
        System.out.println("Numero de Asiento: " + numeroAsiento);
    }
    
    // Metodo estatico para cuando se genera una reserva, es decir: se cambia el estado del asiento en el horario indicado en dicho evento

    public static void reservar(ArrayList <Evento> eventos, String e, LocalDate f, LocalTime h, int num) {
        for(int i=0; i < eventos.size(); i++){
            if(eventos.get(i).getNombreEvento() == e){
                for(int j=0; j < eventos.get(i).getHorarios().size(); j++){
                    if(eventos.get(i).getHorarios().get(j).getFecha() == f && eventos.get(i).getHorarios().get(j).getHora() == h){
                        eventos.get(i).getHorarios().get(j).setAsientoX(num);
                    }
                }
            }
        }
    }

}
