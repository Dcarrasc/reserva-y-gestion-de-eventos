public class Asiento{                               // Clase para individualizar los asientos
    private int numero;                             // El numero que corresponde del asiento
    private boolean ocupado;                        // Si es que el asiento esta ocupado o no
   
    public Asiento(int n, boolean o){               // Constructor de la clase
        this.numero = n;
        this.ocupado = o;
    }

    public int getNumero(){                         // Metodo para obtener el numero del asiento
        return this.numero;
    }

    public boolean getOcupado(){                    // Metodo para obtener el estado del asiento (libre u ocupado)
        return this.ocupado;
    }
    
    public void setOcupado(boolean o){              // Metodo para cambiar el estado del asiento
        this.ocupado = o;
    }

    public void printAsiento(){                     // Metodo para imprimir el numero y el estado del asiento
        System.out.print("("+numero+","+ocupado+") ");
    }
}
