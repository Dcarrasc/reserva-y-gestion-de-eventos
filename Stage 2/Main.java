import java.time.LocalDate;             // Librerias usadas
import java.time.LocalTime;
import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {                     // Clase Main
    public static void main(String[] args) {

        // Arreglos globales para guardar los eventos, las reservas y los usuarios
        ArrayList <Evento> eventos = new ArrayList <Evento> ();
        ArrayList <Reserva> reservas = new ArrayList <Reserva> ();
        ArrayList <Usuario> usuarios = new ArrayList <Usuario> ();

        // Se crean y agregan este evento y horario solamente para hacer pruebas
        Evento eve = new Evento("Concierto al aire libre", 1500);
        LocalDate fecha = LocalDate.parse("2018-10-30");
        LocalTime hora = LocalTime.of(17, 30);
        Horario h = new Horario(fecha, hora, 10);
        eve.addHorario(h);
        eventos.add(eve);

        // Se lee el archivo de usuarios guardados y se agregan al array de usuarios
        Lectura.leerArchivoUsuarios("Usuarios.txt", usuarios);

        // Aqui es donde comienza todo el programa

        Funciones.MensajeBienvenida();      // Se da el mensaje de bienvenida
        while(true){                        // Se crea el ciclo repetitivo con while(true)
            Funciones.MenuPrincipal();      // Se muestra el menu principal
            switch (Funciones.getOpcion()) {    //Dependiendo la opcion que se elige, se tienen dos casos
                case 1: // Para entrar como cliente
                    // Se crea un usuario con los datos que se proporcionan
                    String nombreCliente = Funciones.obtenerNombreUsuario();
                    String claveCliente = Funciones.obtenerClaveUsuario();
                    Usuario userCli = new Usuario(nombreCliente, claveCliente, 1);
                    // Si el usuario pasa la verificacion, entra al if
                    if(Funciones.verificadorUsuario(userCli.getNombreUsuario(), userCli.getClaveUsuario(), userCli.getTipoUsuario(), usuarios)){
                        Funciones.MenuCliente();        // Se muestra el menu del cliente
                        switch (Funciones.getOpcion()) {    // Dependiendo la opcion que se elige, el cliente puede hacer dos cosas
                            case 1: // Hacer una reserva
                                Funciones.OpcionHacerReserva(eventos, reservas, userCli);  
                                break;
                    
                            case 2: // Ver una reserva
                                Funciones.OpcionVerReserva(reservas);
                                break;
                        }
                    }
                    else{
                        // Si el usuario no paso la verificacion, le aparecera un mensaje de error y volvera al Menu Principal
                        System.out.println();
                        System.out.println("El nombre de usuario o la clave son incorrectos");
                    }
                    break;
            
                case 2: // Para entrar como Administrador
                    // Se crea un usuario con los datos que se proporcionan
                    String nombreAdministrador = Funciones.obtenerNombreUsuario();
                    String claveAdministrador = Funciones.obtenerClaveUsuario();
                    Usuario userAdmin = new Usuario(nombreAdministrador, claveAdministrador, 2);
                    // Si el usuario pasa la verificacion, entra al if
                    if(Funciones.verificadorUsuario(userAdmin.getNombreUsuario(), userAdmin.getClaveUsuario(), userAdmin.getTipoUsuario(), usuarios)){
                        Funciones.MenuAdministrador();
                        switch (Funciones.getOpcion()) {
                            case 1:
                                Funciones.OpcionAgregarEvento(eventos);
                                break;
                    
                            case 2:
                                Funciones.OpcionAgregarHorario(eventos);
                                break;

                            case 3:
                                Funciones.OpcionCambiarPrecio(eventos);
                                break;

                            case 4:
                                Funciones.OpcionTotalEntradasHorario(eventos);
                                break; 
                        }
                    }

                    else{
                        // Si el usuario no paso la verificacion, le aparecera un mensaje de error y volvera al Menu Principal
                        System.out.println();
                        System.out.println("El nombre de usuario o la clave son incorrectos");
                    }
                    break;
                
                case 3: // Para crear nuevo usuario
                    // Se llama la funcion para crear el nuevo usaurio y a gregarlo al array
                    Funciones.MenuNuevoUsuario(usuarios);
                    break;

                case 4: // Para cerrar el programa
                    Escritura.EscribirArchivoUsuarios(usuarios);
                    System.exit(0);

            }
            
        }
        
        
    }

}
