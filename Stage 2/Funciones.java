import java.util.ArrayList;                 // Librerias usadas
import java.util.Scanner;
import java.time.LocalDate;
import java.time.LocalTime;

public class Funciones {                    // Clase solamente para generar funciones estaticas usadas en el main u otra clase
    
    public Funciones(){}                    // Constructor de la clase vacio

    // Metodo para imprimir el mensaje de Bienvenida
    public static void MensajeBienvenida(){
        System.out.println();
        System.out.println("Bienvenido al Programa de Gestion y Reserva de Eventos!");
    }

    // Metodo para imprimir el Menu Principal
    public static void MenuPrincipal(){
        System.out.println();
        System.out.println("Por favor indique si es Cliente, Administrador o Nuevo Usuario");
        System.out.println();
        System.out.println("1. CLiente");
        System.out.println("2. Administrador");
        System.out.println("3. Nuevo Usuario");
        System.out.println("4. Salir");
        System.out.println();
        System.out.print("Opcion: ");
    }

    // Metodo para obtener texto ingresado como String y pasado a int
    public static int getOpcion(){
        String opcionString = "";
        Scanner entrada = new Scanner(System.in);
        opcionString = entrada.nextLine();
        int opcion = Integer.parseInt(opcionString);
        //entrada.close();
        return opcion;
    }

    // Metodo para obtener texto ingresado como String
    public static String getOpcionString(){
        String opcion = "";
        Scanner entrada = new Scanner(System.in);
        opcion = entrada.nextLine();
        //entrada.close();
        return opcion;
    }

    // Metodo para imprimir el Menu de Usuario
    public static void MenuCliente(){
        System.out.println();
        System.out.println("Has entrado como Cliente");
        System.out.println();
        System.out.println("Indica la opcion que quieres");
        System.out.println();
        System.out.println("1. Hacer una reserva");
        System.out.println("2. Ver mi reserva");
        System.out.println();
        System.out.print("Opcion : ");      
    }

    // Metodo para hacer una reserva
    // Se recibe como parametros los arreglos globales de eventos y reservas, ya que se recorre el arreglo de eventos y se modifica el arreglo de reservas
    public static void OpcionHacerReserva(ArrayList <Evento> eve, ArrayList <Reserva> res, Usuario user){
        System.out.println();
        System.out.println("A que evento quieres ir?");     // Se hace la pregunta del evento a seleccionar
        System.out.println();
        for(int i=0; i<eve.size();i++){             // Se recorre el arreglo de eventos para mostrarlos en pantalla
            System.out.println((i+1)+". " + eve.get(i).getNombreEvento());
        }
        System.out.println();
        System.out.print("Opcion: ");
        int opcEvento = Funciones.getOpcion();      // Se guarda la opcion indicada del evento
        Evento eventoElegido = eve.get(opcEvento - 1);
        System.out.println();
        System.out.println("El evento "+ eventoElegido.getNombreEvento() + " tiene un precio de $" + eventoElegido.getPrecioEvento());
        System.out.println();
        System.out.println("En que horario quieres ir?");   // Ahora se hace la pregunta del horario a seleccionar
        System.out.println();
        for(int j=0;j<eventoElegido.getHorarios().size();j++){  // Se recorre el arreglo de horarios del evento indicado para mostrarlos en pantalla
            System.out.print((j+1)+ ". ");
            eventoElegido.getHorarios().get(j).printHorarioFechaHora();
            System.out.println();
        }
        System.out.println();
        System.out.print("Opcion: ");
        int opcHorario = Funciones.getOpcion();     // Se guarda la opcion indicada del horario
        Horario horarioElegido = eventoElegido.getHorarios().get(opcHorario - 1);
        System.out.println();
        System.out.print("Ud ha seleccionado el Horario ");
        horarioElegido.printHorarioFechaHora();
        System.out.println();
        System.out.println();
        System.out.println("Seleccione su asiento");    // Ahora se pregunta por el asiento a seleccionar
        System.out.println();
        for(int k=0; k<horarioElegido.getAsientos().size();k++){    // Se recorre el arreglo de asientos de ese horario en ese evento para mostrarlos en pantalla
            System.out.print("Asiento " + (k+1) + " esta " );
            if(horarioElegido.getAsientos().get(k).getOcupado()){
                System.out.println("OCUPADO");
            }
            else{
                System.out.println("LIBRE");
            }
        }
        System.out.println();
        System.out.print("Numero de asiento: ");
        int opcAsiento = Funciones.getOpcion();         // Se guarda la opcion indicada del asiento
        
        // Se hace la reserva: se cambia el estado del numero de asiento indicado en ese horario en ese evento
        Reserva.reservar(eve, eventoElegido.getNombreEvento(), horarioElegido.getFecha(), horarioElegido.getHora(), opcAsiento);
        
        // Se crea una reserva y se agrega al arreglo global de reservas
        Reserva reservaRealizada = new Reserva(eventoElegido, horarioElegido.getFecha(), horarioElegido.getHora(), opcAsiento, user);
        res.add(reservaRealizada);
        
        System.out.println();
        System.out.println("Los datos han sido guardados y actualizados con exito!");
        System.out.println();
        System.out.println("Su numero de reserva es: " + reservaRealizada.getIdReserva());
        System.out.println();
        System.out.println("Muchas gracias por hacer su reserva con nosotros :)");
        System.out.println();
        System.out.println("**************************************************************************");

    }

    // Metodo para ver una reserva
    // Recibe como parametro el arreglo de reservas, porque debe buscar en el para encontrar los datos de la reserva
    public static void OpcionVerReserva(ArrayList <Reserva> res){
        System.out.println();
        System.out.print("Por favor indique su numero de reserva: ");   // Se pregunta por el numero de reserva
        int numReserva = Funciones.getOpcion();                         // Se obtiene el numero de reserva
        Reserva reservaIndicada = res.get(numReserva-1);                // Se busca en el arreglo
        System.out.println();
        System.out.println("Sus datos de reserva son los siquientes");
        System.out.println();
        reservaIndicada.printReserva();                                 // Imprime los datos de la reserva indicada
        System.out.println();
        System.out.println("**************************************************************************");
    }

    // Metodo para imprimir el Menu de Administrador
    public static void MenuAdministrador(){
        System.out.println();
        System.out.println("Has entrado como Administrador");
        System.out.println();
        System.out.println("Indica la opcion que quieres");
        System.out.println();
        System.out.println("1. Agregar un evento");
        System.out.println("2. Agregar un horario");
        System.out.println("3. Cambiar precio de evento");
        System.out.println("4. Total de entradas reservadas para un horario");
        System.out.println();
        System.out.print("Opcion : ");
    }

    // Metodo para agregar un evento
    // Tiene como parametro el arreglo de eventos que se modificara, agregando uno nuevo
    public static void OpcionAgregarEvento(ArrayList <Evento> even){
        System.out.println();
        System.out.print("Indique el nombre del Evento: ");     // Se pregunta el nombre del evento
        String nombreEvento = Funciones.getOpcionString();      // Se guarda el nombre del evento
        System.out.println();
        System.out.print("Indique el precio del Evento: ");     // Se pregunta el precio del evento
        int precioEvento = Funciones.getOpcion();               // Se guarda el precio del evento
        Evento nuevoEvento = new Evento(nombreEvento, precioEvento);    // Se crea el nuevo evento
        even.add(nuevoEvento);         // Se agrega el evento al arreglo
        System.out.println();
        System.out.println("Se ha guardado con exito el evento " + nombreEvento + " con el precio de $" + precioEvento);
        System.out.println();
        System.out.println("**************************************************************************");

    }

    // Metodo para agregar un horario
    // Tiene como parametro el arreglo de eventos, porque se debe recorrer y modificar
    public static void OpcionAgregarHorario(ArrayList <Evento> even){
        System.out.println();
        System.out.println("A que evento desea agregar un nuevo horario?");     // Se pregunta por el evento a seleccionar
        System.out.println();
        for(int i=0; i<even.size();i++){            // Se recorre el arreglo de ventos para mostrar por pantalla
            System.out.println((i+1)+". " + even.get(i).getNombreEvento());
        }
        System.out.println();
        System.out.print("Opcion: ");
        int opcEvento = Funciones.getOpcion();      // Se obtiene la opcion indicada
        System.out.println();
        System.out.print("Indique la fecha para el evento en formato 'AAAA-MM-DD' : "); // Se pregunta por la fecha para el nuevo horario
        LocalDate fecha = LocalDate.parse(Funciones.getOpcionString());     // Se guarda la fecha indicada
        System.out.println();
        System.out.print("Indique la hora para el evento en formato 'HH:MM' : ");       // Se pregunta por la hora para el nuevo horario
        LocalTime hora = LocalTime.parse(Funciones.getOpcionString());      // Se guarda la hora indicada
        System.out.println();
        System.out.print("Indique la cantidad de asientos para este horario: ");        // Se pregunta por la cantidad de asientos para el horario indicado
        int cantidadAsientos = Funciones.getOpcion();                       // Se guarda la cantidad indicada
        Horario nuevoHorario = new Horario(fecha, hora, cantidadAsientos);  // Se crea el nuevo horario
        even.get(opcEvento-1).addHorario(nuevoHorario);                     // Se agrega al arreglo en el evento indicado
        System.out.println();
        System.out.print("Se ha agregado al evento " + even.get(opcEvento-1).getNombreEvento() + " el nuevo horario ");
        System.out.print("con fecha ");
        System.out.print(fecha);
        System.out.print(" hora ");
        System.out.print(hora);
        System.out.print(" y una cantidad de " + cantidadAsientos + " asientos disponibles");
        System.out.println();
        System.out.println();
        System.out.println("**************************************************************************");
    }

    // Metodo para cambiar el precio de un evento
    // Tiene como parametro un arreglo de eventos, porque se modificara uno de ellos
    public static void OpcionCambiarPrecio(ArrayList <Evento> even){
        System.out.println();
        System.out.println("A que evento quieres cambiarle el precio?");    // Se pregunta por el evento a ser cambiado
        System.out.println();
        for(int i=0; i<even.size();i++){                                    // Se recorre el arreglo de eventos para ser mostrados por pantalla
            System.out.println((i+1)+". " + even.get(i).getNombreEvento());
        }
        System.out.println();
        System.out.print("Opcion: ");
        int opcEvento = Funciones.getOpcion();          // Se obtiene la opcion indicada
        System.out.println();
        System.out.print("Indique el nuevo precio del evento: ");       // Se pregunta por el nuevo precio del evento indicado
        int nuevoPrecio = Funciones.getOpcion();                        // Se guarda el nuevo precio
        even.get(opcEvento-1).setPrecio(nuevoPrecio);                   // Se modifica el evento indicado con el nuevo precio
        System.out.println();
        System.out.println("El nuevo precio del Evento " + even.get(opcEvento-1).getNombreEvento() + " es $" + nuevoPrecio);
        System.out.println();
        System.out.println("**************************************************************************");
    }

    // Metodo para obtener la cantidad de entradas reservadas en un horario
    // Tiene como parametros el arreglo de eventos que sera recorrido, el evento que se desea ver y el horario a analizar
    public static int EntradasReservadasHorario(ArrayList <Evento> even, Evento e, Horario hor){
        int contadorAsientosOcupados = 0;       // Contador para los asientos que estan ocupados
        for(int i=0; i<even.size();i++){        // Se recorre el arreglo de eventos
            if(even.get(i).getNombreEvento() == e.getNombreEvento()){   // Se encuentra el evento indicado
                for(int j=0;j<even.get(i).getHorarios().size();j++){    // Se recorre el arreglo de horarios del evento indicado
                    if(even.get(i).getHorarios().get(j).getFecha()==hor.getFecha() && even.get(i).getHorarios().get(j).getHora()==hor.getHora()){   // Se encuentra el horario indicado
                        for(int k=0; k<even.get(i).getHorarios().get(j).getAsientos().size(); k++){ // Se recorre el arreglo de asientos en el horario del evento indicado
                            if(even.get(i).getHorarios().get(j).getAsientos().get(k).getOcupado()){ // Se encuentra los asientos ocupados
                                contadorAsientosOcupados++;     // Se aumenta el contador de asientos ocupados
                            }
                        }
                    }
                }
            }
        }
        return contadorAsientosOcupados;    // Retorna la cuenta final de asientos ocupados en dicho evento y horario
    }

    // Metodo para ver el total de entradas reservadas en un horario (a nivel de usuario)
    // Tiene como parametro el arreglo de eventos, ya que sera recorrido para encontrar el horario y los asientos ocupados
    public static void OpcionTotalEntradasHorario(ArrayList <Evento> even){
        System.out.println();
        System.out.println("Indique el evento");        // Se pregunta el evento a seleccionar
        System.out.println();
        for(int i=0; i<even.size();i++){                // Se recorre el arreglo de eventos para mostrarlos por pantalla
            System.out.println((i+1)+". " + even.get(i).getNombreEvento());
        }
        System.out.println();
        System.out.print("Opcion: ");
        int opcEvento = Funciones.getOpcion();          // Se guarda el evento indicado
        Evento eventoElegido = even.get(opcEvento-1);
        System.out.println();
        System.out.println("Indique el horario");       // Se pregunta el horario a seleccionar de dicho evento
        System.out.println();
        for(int j=0; j<even.get(opcEvento-1).getHorarios().size();j++){ // Se recorre el arreglo de horarios para mostrarlos por pantalla
            System.out.print((j+1)+ ". ");
            eventoElegido.getHorarios().get(j).printHorarioFechaHora();
            System.out.println();
        }
        System.out.println();
        System.out.print("Opcion: ");
        int opcHorario = Funciones.getOpcion();         // Se guarda la opcion indicada
        Horario horarioElegido = eventoElegido.getHorarios().get(opcHorario-1); // Se guarda el horario indicado
        int totalEntradas = EntradasReservadasHorario(even, eventoElegido, horarioElegido); // Se llama a la funcion definida arriba con el evento y el horario elegidos
        System.out.println();
        System.out.print("En el evento " + eventoElegido.getNombreEvento() + " en el horario ");
        horarioElegido.printHorarioFechaHora();
        System.out.println(" hay " + totalEntradas + " entradas reservadas");
        System.out.println();
        System.out.println("**************************************************************************");
    }

    public static void MenuNuevoUsuario(ArrayList <Usuario> usu){
        System.out.println();
        System.out.println("Registrate como nuevo usuario");
        String nombreUsuario = obtenerNombreUsuario();
        String claveUsuario = obtenerClaveUsuario();
        System.out.println();
        System.out.print("Indique si se quiere registrar como Cliente(1) o como Administrador(2): ");
        int tipoUsuario = Funciones.getOpcion();
        usu.add(new Usuario(nombreUsuario, claveUsuario, tipoUsuario));
        System.out.println();
        System.out.println("Se ha creado el nuevo usuario con exito!");
        System.out.println();
        System.out.println("**************************************************************************");
    }

    // Metodo para obtener el nombre de usuario
    public static String obtenerNombreUsuario(){
        System.out.println();
        System.out.print("Indique su nombre de usuario: ");     // Se pregunta el nombre de usuario
        String nombre = Funciones.getOpcionString();            // Se guarda el nombre de usuario
        return nombre;      // Se retorna el nombre de usuario
    }

    // Metodo para obtener la clave del usuario
    public static String obtenerClaveUsuario(){
        System.out.println();
        System.out.print("Indique su clave: ");                 // Se pregunta la clave del usuario
        String clave = Funciones.getOpcionString();             // Se guarda la clave del usuario
        String claveEncriptada = Hash.getHash(clave, "SHA1");   // Se guarda la clave encriptada
        return claveEncriptada;         // Se retorna la clave encriptada del usuario
    }

    // Metodo para verificar si un usuario existe 
    // Tiene como parametros el nombre, la clave (encriptada) y el tipo de usuario a verificar, junto con el arreglo con todos los usuarios existentes
    public static boolean verificadorUsuario(String nombre, String clave, int tipo, ArrayList <Usuario> usu){
        for(int i=0; i<usu.size(); i++){        // Se recorre el arreglo de usuarios para buscar el usuario indicado
            if(usu.get(i).getNombreUsuario().equals(nombre) && usu.get(i).getClaveUsuario().equals(clave) && usu.get(i).getTipoUsuario() == tipo){    // Se encuentra el usuario indicado
                return true;        // Devuelve true, indicando que si esta verificado (aparece en el arreglo)
            }
        }
        // Si no aparece, retorna false
        return false;
    }
}
