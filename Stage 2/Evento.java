import java.time.LocalDate;                                 // Librerias usadas
import java.time.LocalTime;
import java.util.ArrayList;

public class Evento {                                       // Clase para Eventos
    private ArrayList <Horario> horarios;                   // Array de los horarios del evento
    private int precioEvento;                               // Precio del evento
    private String nombreEvento;                            // Nombre del evento

    public Evento(String nombre, int precio){               // Constructor
        this.nombreEvento = nombre;
        this.precioEvento = precio;
        horarios = new ArrayList <Horario> ();              // Se inicializa el array de horarios como vacio
    }

    public String getNombreEvento(){                        // Metodo para obtener el nombre del evento
        return this.nombreEvento;
    }

    public int getPrecioEvento(){                           // Metodo para obtener el precio del evento
        return this.precioEvento;
    }

    public void setPrecio(int precio){                      // Metodo para cambiar el precio del evento
        this.precioEvento = precio;
    }

    public ArrayList<Horario> getHorarios(){                // Metodo para obtener el array de horarios del evento
        return this.horarios;
    }

    public Horario getHorarioX(LocalDate f, LocalTime h){   // Metodo para obtener el horario con fecha y hora especifica
        for(int i=0; i<horarios.size();i++){
            if(horarios.get(i).getFecha()==f && horarios.get(i).getHora()==h){
                return this.horarios.get(i);
            }
        }
        return null;
    }

    public void addHorario(Horario horario){                // Metodo para agregar un horario al evento
        this.horarios.add(horario);
    }

    public void printEvento(){                                  // Metodo para imprimir el evento y sus horarios
        System.out.print(nombreEvento+","+precioEvento+",[");   // nombreEvento, fechaEvento, [(fecha1 , hora1) ...]
        for(int i=0;i<horarios.size();i++){
            horarios.get(i).printHorarioFechaHora();
        }
        System.out.println("]");
    }

}
